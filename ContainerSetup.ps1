﻿$LogTime = Get-Date -Format "MM-dd-yyyy_hh-mm-ss"
$ErrorActionPreference="SilentlyContinue"
Stop-Transcript | out-null
$ErrorActionPreference = "Continue"
Start-Transcript -path D:\Logs\Container_Setup\$Logtime+$ContainerName"_ContainerSetup_output.txt" -append

## Set Variables
$ContainerName = Read-Host -Prompt 'Type a Container Name'

## Prep Docker Container Host
Write-Host " ## Prep Docker Host Container"

## Install Docker
Write-Host " ## Install Docker"
Install-windowsfeature -Name Containers
Install-Module -Name DockerMsftProvider -Repository PSGallery -Force

## Install Base IIS
Write-Host " ## Install IIS"
Install-WindowsFeature -Name Web-server

## Install Windows Auth
Write-Host " ## Install Win Auth"
Install-WindowsFeature -Name web-windows-auth

## Install GMSA on Container Host
Write-Host " ## Install GMSA on Container Host"
## Enable AD Powershell
Enable-WindowsOptionalFeature -FeatureName ActiveDirectory-Powershell -online -all
Write-Host " ## AD Powershell enbabled"

## Import AD Module
Import-Module ActiveDirectory
Write-Host " ## AD module imported"

## Find GMSA in AD
Write-Host " ## Get GMSA Info"
Get-ADServiceAccount -identity GMSAIIS

## Install GMSA on Container Host
Write-Host " ## Install GMSA on Container Host"
Install-ADServiceAccount -Identity GMSAIIS

## Test Service Account
Write-Host " ## Test Service Account"
Test-ADServiceAccount -Identity GMSAIIS

## move credential file from working docker host to default location
Write-Host " ## Copy Item from spbdocker01 to local\CredentialSpecs\ location"
Copy-Item -Path '\\spbdocker01\C$\programdata\docker\credentialspecs\GMSA_IIS.json' 'C:\programdata\docker\credentialspecs\GMSA_IIS.json'

## Docker Version / Info
Write-Host " ## Docker Info"
docker info

## Docker Pull WinAuth Image"
## Write-Host " ## Docker Pull Cheese WinAuth Image"
## docker pull artisticcheese/winauth:iis

## Docker Creat Volume
## Write-Host " ## create volume"
## docker volume create

##Build Docker image
## files are store for the winauth build D:\Containers\ASPNETWinAuth
## Set-Location to D:\containers\aspnetwinauth
Set-Location -path D:
Set-Location -path D:\containers\ASPNETWinAuth
Write-Host " ## Changed Directory and Building Docker Image"
docker build -t jefftag .

## Docker list image
Write-Host " ## Docker list images"
docker images -a

## Build Docker Container
Write-Host " ## Building Docker Container"
docker run -it -h $ContainerName --security-opt "credentialspec=file://gmsa_iis.json" -d -p 8884:80  -e auth=Windows  --name $ContainerName jefftag

## Waiting for container to build
Write-Host " ## Waiting 60 Seconds for Container to build"
Start-sleep -Seconds 60
Write-Host " ## 60 Seconds has ended"

## Copy script to docker container
Set-Location -path D:\scripts
docker cp .\Post_Container_Setup.ps1 ContainerWinAuth:c:\inetpub\wwwroot
docker exec $ContainerName powershell -command c:\inetpub\wwwroot\Post_Container_Setup.ps1

## Run container commands in powershell administrator not ISE
Write-Host " ## Executing Post Container Setup"

## Waiting for container to build
Write-Host " ## Waiting 60 Seconds for Container to be CONFIGURED"
Start-sleep -Seconds 60
Write-Host " ## 60 Seconds has ended"

## Copy logfile to local host
Write-Host " ## Copy Container Log back to host"
docker cp ${ContainerName}:C:\inetpub\wwwroot\Container_PostSetup_output.txt D:\logs\Container_Config\${LogTime}+${ContainerName}"Container_PostSetup_output.txt"

Write-Host " ## Check ${ContainerName}Container_PostSetup_output.txt"

## Stop Transcript
Stop-Transcript