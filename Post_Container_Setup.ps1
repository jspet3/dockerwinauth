﻿$ErrorActionPreference="SilentlyContinue"
Stop-Transcript | out-null
$ErrorActionPreference = "Continue"
Start-Transcript -path "Container_PostSetup_output.txt" -append

Write-Host " ## Module Web Admin"
Import-Module WebAdministration

Write-Host " ## Getting what the pool is running under"
Get-ItemProperty IIS:\AppPools\DefaultAppPool -name processModel.identityType

Write-Host " ## Setting IIS Pool to run under NetworkService"
Set-ItemProperty IIS:\AppPools\DefaultAppPool -name processModel.identityType -value 2

Write-Host " ## Waiting 15 Seconds"
Start-Sleep -Seconds 15

Write-Host " ## Pool should now be set to run as NETWORk SERVICE"
Get-ItemProperty IIS:\AppPools\DefaultAppPool -name processModel.identityType

Write-Host " ## Testing AD connnection"
NLTEST /sc_verify:ntregis.regis.local

Write-Host " ## Testing DFS Access"
Dir \\ntregis.regis.local\sysvol

Write-Host " ## Checking for web-server feature on Container"
Get-windowsfeature -name web-server

Write-Host " ## Checking for Windows Authentication feature on Container"
Get-windowsfeature -name web-windows-auth

Write-Host " ## Installing Windows Authentication feature on Container"
Install-windowsfeature -name web-windows-auth

## Disable Anonymous Auth for Default Web Site:
Write-Host " ## Disable Anonymous Auth for Default Web Site" 
Set-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -location 'Default Web Site' -filter "system.webServer/security/authentication/anonymousAuthentication" -name "enabled" -value "False"

## Enable Windows Auth for Default Web Site:
Write-Host " ## Enable Windows Auth for Default Web Site"
Set-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -location 'Default Web Site' -filter "system.webServer/security/authentication/windowsAuthentication" -name "enabled" -value "True"
 
## Reconfigure Default App Pool to use Network Service, as required by documentation specifically for Docker w/ GMSA:
Write-Host " ## Reconfigure Default App Pool to use Network Service"
Set-WebConfigurationProperty -pspath 'MACHINE/WEBROOT/APPHOST' -filter "system.applicationHost/applicationPools/add[@name='DefaultAppPool']/processModel" -name "identityType" -value "NetworkService"


## Stop Transcript
Write-Host " ## Transcript has STOPPED"

Stop-Transcript
